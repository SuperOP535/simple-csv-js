var CSV = (typeof module !== 'undefined' ? require('./index') : CSV);

var input = 'hello,world\n' +
            'how,are\n' +
            'you,world';

console.log('input:\n', input, '\n');

var parsed = CSV.parse(input);
console.log('parsed:\n', parsed, '\n');

var stringified = CSV.stringify(parsed);
console.log('stringified:\n', stringified, '\n');

console.log('success?', input === stringified);