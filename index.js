(function() {
    var CSV = {
        parse: function(string) {
            if(typeof string !== 'string') throw new TypeError('String expected');
            var output = [];
            var lines = string.trim().split(/\r?\n/);
            lines.forEach(function(line) {
                output.push(line.split(','));
            });
            return output;
        },
        stringify: function(array) {
            if(!Array.isArray(array)) throw new TypeError('Array expected');
            var lines = [];
            array.forEach(function(entry) {
                if(!Array.isArray(array)) throw new TypeError('The array must only contain arrays');
                lines.push(entry.join(','));
            });
            return lines.join('\n');
        }
    };
    if(typeof module !== 'undefined')
        module.exports = CSV;
    else window.CSV = CSV;
})();